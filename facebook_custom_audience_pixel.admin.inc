<?php

/**
 * @file
 * Administrative page callbacks for the facebook_custom_audience_pixel module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function facebook_custom_audience_pixel_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['facebook_custom_audience_pixel_id'] = array(
    '#title' => t('Custom Audience Pixel-ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('facebook_custom_audience_pixel_id', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('This ID is Facebook Advertiser account. To get a Pixel-ID, <a href="@analytics">Create an Facebook Ad Account</a>, and get the accounts Pixel-ID: <a href="@pixelid">Pixel-ID</a>.', array('@analytics' => 'http://www.facebook.com/ads/manage', '@pixelid' => url('https://www.facebook.com/ads/manager/data_sources/pixels/', array('fragment' => 'webProperty')))),
  );
  
  return system_settings_form($form);
  
}